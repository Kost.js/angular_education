import { Component } from '@angular/core';

interface Nav {
  path: string,
  name: string,
  exact: boolean
}

@Component({
  selector: 'app-root',
  styleUrls: ['app.component.scss'],
  template: `
    <div class="app">
      <h1>{{ title }}</h1>
    <!-- <passenger-dashboard></passenger-dashboard> -->
    <!--<passenger-viewer></passenger-viewer>-->
    <div class="nav">
    <a
    *ngFor="let it of nav"
    [routerLink]="it.path"
    routerLinkActive="active"
    [routerLinkActiveOptions]="{ exact: it.exact }"
    >
    {{ it.name }}
    </a>
    </div>
    <router-outlet></router-outlet>
    </div>

   <!-- <app-two-way></app-two-way> -->
  `
})
export class AppComponent {
  title: string;
  nav: Nav[] = [
    {
      path: '/',
      name: 'Home',
      exact: true
    },
    {
      path: '/passengers',
      name: 'Passengers',
      exact: false
    },
    {
      path: '/not-exist',
      name: '404',
      exact: false
    }
  ]
  constructor() {
    this.title = 'Air passenger list';
  }

}
