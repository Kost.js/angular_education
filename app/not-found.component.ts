import { Component} from '@angular/core';
@Component({
    selector: 'app-not-found',
    template: `
    <div>Page not found. Please go to <a routerLink="/">Home page</a></div>
    `
})

export class NotFoundComponent {}