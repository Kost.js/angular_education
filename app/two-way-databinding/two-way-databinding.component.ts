import { Component } from '@angular/core';

@Component({
    selector: 'app-two-way',
    template: `
    <input type="text"
    [ngModel]="name"
    (ngModelChange)="handleChange($event)"
    >
    <input type="text" [(ngModel)]="name">
    <h1> {{ name }}</h1>
    <input type="text" #inputData >
    <div *ngIf="name">
    Searching for ... {{ name }}
    </div>
    <template [ngIf]="name">
    <div>
    Searching for ... {{ name }}
    </div>
    </template>
    <button (click)="handleClick(inputData.value)">click<button>

    `
})

export class TwoWayDatabindingComponent {
    name: string;
    constructor() {
    }

    handleChange(val: string) {
        this.name = val;
    }

    handleClick(val: string) {
        console.log(val);
    }
}