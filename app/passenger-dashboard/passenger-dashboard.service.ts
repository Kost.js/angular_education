import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Passenger } from './models/passenger.interface';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

const PASSENGER_API: string = '/api/passengers';

@Injectable()
export class PassengerDashboardService {
    constructor(private http: Http) {}
    getPassengers(): Observable<Passenger[]> {
        return this.http.get(PASSENGER_API)
        .map((response: Response) => response.json())
        .catch((err: any) => Observable.throw(err.json()));
    }

    getPassenger(id: number): Observable<Passenger> {
        return this.http.get(`${PASSENGER_API}/${id}`)
        .map((response: Response) => response.json())
        .catch((err: any) => Observable.throw(err.json));
    }

    updatePassenger(passenger: Passenger): Observable<Passenger> {
        let headers = new Headers({
            'Content-type': 'application/json'
        });
        let options = new RequestOptions({
            'headers': headers
        })
        return this.http.put(`${PASSENGER_API}/${passenger.id}`, passenger, options)
        .map((response: Response) => response.json())
        .catch((err: any) => Observable.throw(err.json()));
    }

    removePassenger(passenger: Passenger): Observable<Passenger> {
        return this.http.delete(`${PASSENGER_API}/${passenger.id}`)
        .map((response: Response) => response.json())
        .catch((err: any) => Observable.throw(err.json()));
    }
}