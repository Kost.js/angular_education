import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { PassengerDashboardService } from '../../passenger-dashboard.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Passenger } from '../../models/passenger.interface';

import 'rxjs/add/operator/switchMap';
@Component({
    selector: 'passenger-viewer',
    styleUrls: [],
    template: `
    <button (click)="goBackToList()">
    Go back
    </button>
    <!-- <div> {{ passenger | json }} </div> -->
   <!-- <div>
       <select (change)="onSelect(passId.value)" #passId>
            <option
                *ngFor="let it of passengers"
                [value]="it.id"
                [selected]="it.id === passenger?.id"
            >
            {{ it.fullName }}
            </option>
                 
        </select>
    </div> -->
    <passenger-form (submitData)="updateData($event)" [detail]="passenger"></passenger-form>
    `
})

export class PassengerViewerComponent implements OnInit, OnChanges {
    passengers: Passenger[];
    passenger: Passenger;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private passengerDashboardService: PassengerDashboardService
        ) {}
    ngOnInit() {
        this.route.params.switchMap((param: Passenger) =>
        this.passengerDashboardService.getPassenger(param.id))
        .subscribe((data: Passenger) => this.passenger = data);
        /*
        this.passengerDashboardService
        .getPassengers()
        .subscribe((data: Passenger[]) => {
            this.passengers = data;
            const [firstIt] = data;
            this.passengerDashboardService
                .getPassenger(firstIt.id)
                .subscribe(
                    (data: Passenger) => {
                    console.log(data)
                    this.passenger = data;   
                });

        });*/
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log(changes);
    }

    updateData(updatePassenger: Passenger) {
        this.passengerDashboardService
        .updatePassenger(updatePassenger)
        .subscribe((data: Passenger) => 
        this.passenger = Object.assign({}, this.passenger, data));
    }

    onSelect(_event: number) {
        console.log('Data', _event);
        this.passengerDashboardService
        .getPassenger(_event)
        .subscribe((data: Passenger) => this.passenger = data);
    }

    goBackToList() {
        this.router.navigate(['/passengers']);
    }
} 