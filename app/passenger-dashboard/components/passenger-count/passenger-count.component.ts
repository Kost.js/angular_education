import { Component, Input } from '@angular/core';
import { Passenger } from '../../models/passenger.interface';
import { EMLINK } from 'constants';
@Component({
    selector: 'passenger-count',
    template: `
    <div>Passenger Count</div>
    <div>{{ getNumberRegisterPassengers() }}/{{ items?.length }}</div>
    `
})

export class PassengerCountComponent {
@Input() items: Passenger[]; 
constructor() {}
    getNumberRegisterPassengers(): number 
    {
        if(!this.items) return;
        return this.items.filter((el: Passenger) => el.checkedIn ).length;
    }
}
