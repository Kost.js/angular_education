import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Passenger } from '../../models/passenger.interface';
import { Baggage } from '../../models/baggage.interface';
import { identifierModuleUrl } from '@angular/compiler';
import { HttpModule } from '@angular/http';
import { Element } from '@angular/compiler/src/ml_parser/ast';
import { isNull } from 'util';
import { NgModel } from '@angular/forms';
@Component({
    selector: 'passenger-form',
    styleUrls: ['passenger-form.component.scss'],
    template: `
    <form (ngSubmit)="onSubmit(form.value, form.valid)"
    #form="ngForm" novalidate>
        Form!
        <div>
        {{ detail | json }}
        </div>
        <div>
        Passenger name:
        <input type="text"
        name="fullName"
        #fullName="ngModel"
        required
        [ngModel]="detail?.fullName"
        >
        </div>
        <div class="error"
        *ngIf="fullName.invalid && fullName.dirty">
        Sorry You input wrong full name
        </div>
        <div>
        Passenger id:
        <input
        type="number"
        name="id"
        #id="ngModel"
        required
        [ngModel]="detail?.id">
        </div>
        <div class="error"
        *ngIf="id.invalid && id.dirty">
        Sorry You input wrong Id
        </div>
        <label>
        <input
        type="radio"
        [value] = "true"
        name="checkedIn"
        [ngModel] = "detail?.checkedIn"
        (ngModelChange)="toggelCheckedIn($event)"
        >
        Yes</label>
        <label>
        <input
        type="radio"
        [value] = "false"
        name="checkedIn"
        [ngModel] = "detail?.checkedIn"
        (ngModelChange)="toggelCheckedIn($event)"
        >
        No
        </label>
        <div>
            <input 
            type="checkbox"
            name="checkedIn"
            [ngModel] = "detail?.checkedIn"
            (ngModelChange)="toggelCheckedIn($event)"
            >
        </div>
        <div *ngIf="form.value.checkedIn">
        <input type="number"
        name="checkedInDate"
        [ngModel]="detail?.checkedInDate"
        >
        </div>
       <div>
        Pick your luggage
        <select
        name="baggage"
        [ngModel]="detail?.baggage"
        >
        <option
        *ngFor="let item of baggage"
        [value]="item.key"
        [selected] = "item.key === detail?.baggage"
        >
        {{ item.value }}
        </option>
        </select>
        </div>

        <div>
        Pick your luggage
        <select
        name="baggage"
        [ngModel]="detail?.baggage"
        >
        <option
        *ngFor="let item of baggage"
        [ngValue]="item.key"
        >
        {{ item.value }}
        </option>
        </select>
        </div>
        <div>
        {{ form.value | json }}
        </div>
        <button 
        type="submit"
        [disabled]="form.invalid"
        >
        Send the form
        </button>
    </form>
    `  
})
export class PassengerFormComponent {
    @Input()
    detail: Passenger;
    @Output()
    submitData: EventEmitter <Passenger> = new EventEmitter();
    baggage: Baggage[] = [
        {key: 'none', value: 'None baggage'},
        {key: 'hold', value: 'Hold baggage'},
        {key: 'hand', value: 'Hand baggage'}
    ];
    toggelCheckedIn(isCheckedIn: boolean) {
        if(isCheckedIn) {
            this.detail.checkedInDate = Date.now();
            console.log(this.detail.checkedInDate)
        }
    }
    onSubmit(passenger: Passenger, isValid: boolean) {
        if(isValid) {
            this.submitData.emit(passenger);
        }
    
    }
}
