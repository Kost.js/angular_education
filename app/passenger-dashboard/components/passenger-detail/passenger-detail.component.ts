import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Passenger } from '../../models/passenger.interface';
import { endTimeRange } from '@angular/core/src/profile/wtf_impl';
import { ThrowStmt } from '@angular/compiler/src/output/output_ast';

@Component({
    selector: 'passenger-detail',
    styleUrls: ['passenger-detail.component.scss'],
    template: `
    <div>Passenger Detail:</div>
    <div>
        <span class="status"
          [ngClass]="{'checked-in': detail.checkedIn }"></span>
        <div *ngIf="editing">
        <input type="text"
        [value]="detail.fullName"
        (input)="onNameChange(name.value)"
         #name />
        </div>
        <div *ngIf="!editing">{{ detail.fullName }}</div>
        <p> {{ detail | json }}</p>
        <div class="date">
        {{ detail.checkedInDate ?
          (detail.checkedInDate | date: yyMMNNd | uppercase) : 'No check in date' }}
        </div>
        <p>Children: {{ detail.children?.length || 0 }}</p>
        <button (click)="toggleEdit()">
        {{ editing ? 'Done' : 'Edit' }}
        </button>
        <button (click)="onRemove()">
        Remove
        </button>
        <button (click)="onView()">
        View
        </button>
    </div>
    `
})

export class PassengerDetailComponent implements OnChanges {
    @Input()
    detail: Passenger;
    @Output()
    remove: EventEmitter<Passenger> = new EventEmitter<Passenger>();
    @Output()
    edit: EventEmitter<Passenger> = new EventEmitter<Passenger>();
    @Output()
    view: EventEmitter<Passenger> = new EventEmitter<Passenger>();
    editing: boolean = false;
    constructor() {}

    ngOnChanges(changes) {
        if(changes.detail) {
            this.detail = Object.assign({}, changes.detail.currentValue);
        }
    }

    onNameChange(name: string) {
        this.detail.fullName = name;
    }

    toggleEdit() {
        if(this.editing) {
            this.edit.emit(this.detail);
        }
        this.editing = !this.editing;
    }

    onRemove() {
        this.remove.emit(this.detail);
    }

    onView() {
        this.view.emit(this.detail);
    }
}